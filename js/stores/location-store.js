var alt = require('../alt');
var LocationActions = require('../actions/location-actions');
var LocationSource = require('../sources/location-source');
var FavoritesStore = require('../stores/favourite-store');
var $ = require('jquery');

class LocationStore {
	constructor() {
		this.locations = [];
		this.favLocations =[];
		this.locationName = '';
		this.files = [];
		this.filesNum = []; 
		this.imgOne = [];
		this.imgTwo = []; 
		this.errorMessage = null;
		this.bindListeners({
		  handleUpdateLocations: LocationActions.UPDATE_LOCATIONS,
		  handleDeleteLocation: LocationActions.DELETE_LOCATION,
		  handleFetchLocations: LocationActions.FETCH_LOCATIONS,
		  handleFetchLocations: LocationActions.SET_FAVORITE,
		  handleLocationsFailed: LocationActions.LOCATIONS_FAILED,
		  setFavorites: LocationActions.FAVORITE_LOCATION,
		  handleCreateLocation: LocationActions.CREATE_LOCATION,
		  handleImgOne: LocationActions.UPDATE_IMAGE_ONE,
		  handleImgTwo: LocationActions.UPDATE_IMAGE_TWO
		});



	    this.exportAsync(LocationSource);
	}
	handleUpdateLocations(locations) {
	  	this.locations = locations;
	  	var favLocs = [];

	  	this.favLocations = locations.map((location, i) => {
	  		// $.each( location, function() {
				if(location.has_favorite == "yes") {
				  	favLocs.push(location);
				}
			// });
	  	})
		this.favLocations = favLocs;

	  /*
	  this.favLocations = locations.map((location, i) => {
	  	if(location.has_favorite == "yes") {
	  		return {
			    id: location.id,
			    name: location.name,
			    has_favorite: location.has_favorite	
		    }  		
	  	}
	  })
*/

	  //this.favLocations = favLocations;
	  this.errorMessage = null;
	  // optionally return false to suppress the store change event
	}
	handleFetchLocations() {
	    // reset the array while we're fetching new locations so React can
	    // be smart and render a spinner for us since the data is empty.
	    this.locations = [{ id: 0, name: 'Abu Dhabi' }];
	    this.favLocations = []
	}
	handleLocationsFailed(errorMessage) {
	    this.errorMessage = errorMessage;
	}
	resetAllFavorites() {
		/*
		this.locations = this.locations.map((location) => {
		    return {
		      id: location.id,
		      name: location.name,
		      has_favorite: location.has_favorite
		    };
		});
*/
	}
	setFavorites(location) {
		this.waitFor(FavoritesStore);

		  var favoritedLocations = FavoritesStore.getState().locations;

		  this.resetAllFavorites();

		  favoritedLocations.forEach((location) => {
		    // find each location in the array
		    for (var i = 0; i < this.locations.length; i += 1) {

		      // set has_favorite to true
		      if (this.locations[i].id === location.id) {
		        this.locations[i].has_favorite = true;
		        break;
		      }
		    }
		});
	}
	handleCreateLocation(name) {
		console.log('did we make it?');
	}
	handleDeleteLocation(name) {
		console.log('he wasnt ready!');
	}	
	getLocation(id) {
	    var { locations } = this.getState();
	    for (var i = 0; i < locations.length; i += 1) {
	      if (locations[i].id === id) {
	        return locations[i];
	      }
	    }

	    return null;
	} 
	handleImgOne(files) {
		console.log(files[0].name);
		this.imgOne.push(files[0]);
	}
	handleImgTwo(files) {
		console.log(files[0].name);
		this.imgTwo.push(files[0]);
	}	
}

module.exports = alt.createStore(LocationStore, 'LocationStore');

