var alt = require('../alt');
var QuestionFormActions = require('../actions/questionform-actions');
var QuestionWallActions = require('../actions/questionwall-actions');
var QuestionFormSource = require('../sources/questionform-source');
var QuestionWallSource = require('../sources/questionwall-source');
var $ = require('jquery');

class QuestionFormStore {
	constructor() {
		this.locationName = '';
		this.imgOne = [];
		this.imgTwo = []; 
		this.allQuestions = [];
		this.errorMessage = null;
		this.bindListeners({
		  handleImgOne: QuestionFormActions.UPLOADING_IMG_ONE,
		  handleImgTwo: QuestionFormActions.UPLOADING_IMG_TWO,
		  handleUpdateCaption: QuestionFormActions.UPDATE_CAPTION,
		  handleCreateQuestion: QuestionFormActions.CREATE_QUESTION,
		  handleFetchQuestions: QuestionWallActions.UPDATE_QUESTIONS
		});


	}
	handleImgOne(files) {
		console.log(files[0].name);
		this.imgOne.push(files[0]);
	}
	handleImgTwo(files) {
		console.log(files[0].name);
		this.imgTwo.push(files[0]);
	}	
	handleUpdateCaption(text) {
		this.locationName = text;
	}
	handleCreateQuestion() {
		this.locationName = '';
		this.imgOne = [];
		this.imgTwo = [];
		QuestionWallActions.fetchQuestions();		
	}
	handleFetchQuestions(questions) {
		this.allQuestions = questions;
	}
}

module.exports = alt.createStore(QuestionFormStore, 'QuestionFormStore');

