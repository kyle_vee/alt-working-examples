var alt = require('../alt');
var $ = require("jquery");


var QuestionWallSource = {
  fetch: function () {
      return new Promise(function (resolve, reject) {
        $.ajax({
          url: 'http://localhost/project/wp-content/themes/project/alt/fx/js/sources/php/questions-fetch.php',
          type: 'POST',
          dataType: 'json',
          accepts: {
              xml: 'text/xml',
              text: 'text/plain'
          },
          data: {},
          success: function(data, status) {
                console.log(data)
                resolve(data)

          }.bind(this),
          error: function(jqXHR, textStatus, errorThrown) {
                // Handle errors here
                console.log('ERRORS: ' + errorThrown);
                // STOP LOADING SPINNER
          }.bind(this)
        })
      });
  }
}

module.exports = QuestionWallSource; 