var alt = require('../alt');
var $ = require("jquery");


var QuestionFormSource = {
  uploadImage: function (files) {
      var data = new FormData();
      $.each(files, function(key, value)
      {
          data.append(key, value);
      });

      return new Promise(function (resolve, reject) {
        $.ajax({
            url: 'http://localhost/project/wp-content/themes/project/alt/fx/js/sources/php/image-upload.php',
            type: 'POST',
            data: data,
            cache: false,
            dataType: 'json',
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            success: function(data, textStatus, jqXHR) {
                console.log(files)
                resolve(files)
            }.bind(this),
            error: function(jqXHR, textStatus, errorThrown)
            {
                // Handle errors here
                console.log('ERRORS: ' + textStatus);
                // STOP LOADING SPINNER
            }.bind(this)
        });
      });
  },
  createQuestion: function (formData) {
      return new Promise(function (resolve, reject) {
        $.ajax({
            url: 'http://localhost/project/wp-content/themes/project/alt/fx/js/sources/php/question-create.php',
            type: 'POST',
            dataType: 'json',
            accepts: {
                xml: 'text/xml',
                text: 'text/plain'
            },
            data: {
              'caption': formData[0].caption,
              'imgOne': formData[0].imgOne,
              'imgTwo': formData[0].imgTwo
            },
            success: function(data, textStatus, jqXHR) {
                console.log(formData)
                resolve(formData)
            }.bind(this),
            error: function(jqXHR, textStatus, errorThrown)
            {
                // Handle errors here
                console.log('ERRORS: ' + textStatus);
                // STOP LOADING SPINNER
            }.bind(this)
        });
      });
  },
  fetchQuestions: function () {
        return new Promise(function (resolve, reject) {
          $.ajax({
            url: 'http://localhost/project/wp-content/themes/project/alt/fx/js/sources/php/questions-fetch.php',
            type: 'POST',
            dataType: 'json',
            accepts: {
                xml: 'text/xml',
                text: 'text/plain'
            },
            data: {},
              success: function(data) {
                  // console.log(data)
                  resolve(data);
              }.bind(this),
              error: function(jqXHR, textStatus, errorThrown)
              {
                  // Handle errors here
                  console.log('ERRORS: ' + errorThrown);
                  // STOP LOADING SPINNER
              }.bind(this)
          })
        });
    }  
};

module.exports = QuestionFormSource; 