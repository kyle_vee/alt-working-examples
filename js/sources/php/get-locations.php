<?php
require_once('../../../../../../../../wp-config.php');

global $wpdb;

$user_id = get_current_user_id();

	$locations = $wpdb->get_results( "SELECT * FROM wp_goat_locations", OBJECT_K );
	
	// Storing all the comments that we will be looping through
	$locationsList = array();

	if (!empty($locations)) {
	  foreach ($locations as $location) {
		$id = $location->id;
		$name = $location->name;
		$hasFavorite = $location->has_favorite;

	    $locationsList[] = array (
	        'id' => $id,
	        'name' => $name,
	        'has_favorite' => $hasFavorite
	    );
	  }
	  $response = json_encode($locationsList);
	} else {
	  echo '<span style="margin-left: 25px;">No chat messages available!</span>';
	}	

	echo $response;
	
?>