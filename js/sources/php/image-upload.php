<?php
require_once('../../../../../../../../wp-config.php');

global $wpdb;

$user_id = get_current_user_id();
 
	
// You need to add server side validation and better error handling here

$data = array();
  
    $error = false;
    $files = array();

    $uploaddir = './uploads/';
    foreach($_FILES as $file)
    {
        if(move_uploaded_file($file['tmp_name'], $uploaddir .basename($file['name'])))
        {
            $files[] = $uploaddir .$file['name'];
        }
        else
        {
            $error = true;
        }
    }
    $data = ($error) ? array('error' => 'There was an error uploading your files') : array('files' => $files);

echo json_encode($data);


?>