<?php
require_once('../../../../../../../../wp-config.php');

global $wpdb;

$user_id = get_current_user_id();

	$locations = $wpdb->get_results( "SELECT * FROM wp_fx_questions", ARRAY_A );
	
	// Storing all the comments that we will be looping through
	$locationsList = array();

	if (!empty($locations)) {
	  foreach ($locations as $location) {
		$caption = $location->caption;
		$img_one = $location->img_one;
		$img_two = $location->img_two;

	    $locationsList[] = array (
	        'caption' => $caption,
	        'img_one' => $img_one,
	        'img_two' => $img_two
	    );
	  }
	  $response = json_encode($locations);
	} else {
	  echo '<span style="margin-left: 25px;">No chat messages available!</span>';
	}	

	echo $response;
	
?>