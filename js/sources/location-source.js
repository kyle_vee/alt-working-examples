var alt = require('../alt');
var $ = require("jquery");
var LocationActions = require('../actions/location-actions');

var mockData = [
  { id: 0, name: 'Abu Dhabi' },
  { id: 1, name: 'Berlin' },
  { id: 2, name: 'Bogota' },
  { id: 3, name: 'Buenos Aires' },
  { id: 4, name: 'Cairo' },
  { id: 5, name: 'Chicago' },
  { id: 6, name: 'Lima' },
  { id: 7, name: 'London' },
  { id: 8, name: 'Miami' },
  { id: 9, name: 'Moscow' },
  { id: 10, name: 'Mumbai' },
  { id: 11, name: 'Paris' },
  { id: 12, name: 'San Francisco' }
];

var LocationSource = {
  fetch: function () {
    // returning a Promise because that is what fetch does.
    return new Promise(function (resolve, reject) {
      // simulate an asynchronous action where data is fetched on
      // a remote server somewhere.
      
      $.ajax({
          url: 'http://localhost/project/wp-content/themes/project/alt/documentation/js/sources/php/get-locations.php', 
          type: 'POST', 
          dataType: 'json',
          accepts: {
              xml: 'text/xml',
              text: 'text/plain'
          },
          data: {},
          success: function(locations) {
            console.log("Here it is: " + locations) 
            resolve(locations)
            // LocationActions.updateLocations(locations);
            // ChatServerActionCreators.receiveAll(rawMessages);
          }.bind(this), 
          error: function(xhr, status, err) {
            alert("oops not happening");
          }.bind(this)
      })  
    });
  },
  createLocation: function (name) {
    // returning a Promise because that is what fetch does.
    return new Promise(function (resolve, reject) {
      // simulate an asynchronous action where data is fetched on
      // a remote server somewhere.
      
      $.ajax({
          url: 'http://localhost/project/wp-content/themes/project/alt/documentation/js/sources/php/create-location.php', 
          type: 'POST', 
          dataType: 'json',
          accepts: {
              xml: 'text/xml',
              text: 'text/plain'
          },
          data: {
            'name': name
          },
          success: function(locations) {
            console.log("new location created") 
            resolve(locations)
            // LocationActions.updateLocations(locations);
            // ChatServerActionCreators.receiveAll(rawMessages);
          }.bind(this), 
          error: function(xhr, status, err) {
            alert("oops not happening");
          }.bind(this)
      })  
    });
  },
  uploadImage: function (files) {
      var data = new FormData();
      $.each(files, function(key, value)
      {
          data.append(key, value);
      });

      return new Promise(function (resolve, reject) {
        $.ajax({
            url: 'http://localhost/project/wp-content/themes/project/alt/documentation/js/sources/php/upload-image.php',
            type: 'POST',
            data: data,
            cache: false,
            dataType: 'json',
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            success: function(data, textStatus, jqXHR) {
                console.log(files)
                resolve(files)
            }.bind(this),
            error: function(jqXHR, textStatus, errorThrown)
            {
                // Handle errors here
                console.log('ERRORS: ' + textStatus);
                // STOP LOADING SPINNER
            }.bind(this)
        });
      });
  },
  createQuestion: function (formData) {
      return new Promise(function (resolve, reject) {
        $.ajax({
            url: 'http://localhost/project/wp-content/themes/project/alt/documentation/js/sources/php/create-question.php',
            type: 'POST',
            dataType: 'json',
            accepts: {
                xml: 'text/xml',
                text: 'text/plain'
            },
            data: {
              'caption': formData[0].caption,
              'imgOne': formData[0].imgOne,
              'imgTwo': formData[0].imgTwo
            },
            success: function(data, textStatus, jqXHR) {
                console.log(formData)
                resolve(formData)
            }.bind(this),
            error: function(jqXHR, textStatus, errorThrown)
            {
                // Handle errors here
                console.log('ERRORS: ' + textStatus);
                // STOP LOADING SPINNER
            }.bind(this)
        });
      });
  },  
  delLocation: function (id) {
    // returning a Promise because that is what fetch does.
    return new Promise(function (resolve, reject) {
      // simulate an asynchronous action where data is fetched on
      // a remote server somewhere.
      
      $.ajax({
          url: 'http://localhost/project/wp-content/themes/project/alt/documentation/js/sources/php/delete-location.php', 
          type: 'POST', 
          dataType: 'json',
          accepts: {
              xml: 'text/xml',
              text: 'text/plain'
          },
          data: {
            'id': id
          },
          success: function(locations) {
            console.log("Location should be deleted") 
            resolve(locations)
            // LocationActions.updateLocations(locations);
            // ChatServerActionCreators.receiveAll(rawMessages);
          }.bind(this), 
          error: function(xhr, status, err) {
            alert("oops not happening");
          }.bind(this)
      })  
    });
  },    
  updateLocation: function (id) {
    // returning a Promise because that is what fetch does.
    return new Promise(function (resolve, reject) {
      // simulate an asynchronous action where data is fetched on
      // a remote server somewhere.
      $.ajax({
          url: 'http://localhost/project/wp-content/themes/project/alt/documentation/js/sources/php/update-locations.php', 
          type: 'POST', 
          dataType: 'json',
          accepts: {
              xml: 'text/xml',
              text: 'text/plain'
          },
          data: {
            'id': id
          },
          success: function(locations) {
            console.log("by all accounts, this should now work as soon as i set up the php file " + id);
            resolve(locations)
            // ChatServerActionCreators.receiveAll(rawMessages);
          }.bind(this), 
          error: function(xhr, status, err) {
            alert("oops not happening");
          }.bind(this)
      })  
    });
  }  
};

module.exports = LocationSource; 