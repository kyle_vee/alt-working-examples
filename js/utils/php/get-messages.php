<?php
require_once('../../../../../../../../wp-config.php');

global $wpdb;

$user_id = get_current_user_id();

	$messages = $wpdb->get_results( "SELECT * FROM wp_fx_messages", OBJECT_K );
	
	// Storing all the comments that we will be looping through
	$messagesList = array();

	if (!empty($messages)) {
	  foreach ($messages as $message) {
		$user_id = $message->user_id;
		$thread_id = $message->thread_id;
		$text = $message->text;

	    $messagesList[] = array (
	        'user_id' => $user_id,
	        'thread_id' => $thread_id,
	        'text' => $text
	    );
	  }
	  $response = json_encode($messagesList);
	} else {
	  echo '<span style="margin-left: 25px;">No chat messages available!</span>';
	}	

	echo $response;
	
?>