var alt = require('../alt');
var QuestionWallSource = require('../sources/questionwall-source');
var $ = require('jquery');
var siteUrl = "http://localhost/project/wp-content/themes/project/alt/fx/js/sources/php/uploads/";


class QuestionWallActions {
	updateQuestions(questions) {
		var mockData = [
		  { id: 0, name: 'Abu Dhabi' },
		  { id: 1, name: 'Berlin' },
		  { id: 2, name: 'Bogota' },
		  { id: 3, name: 'Buenos Aires' },
		  { id: 4, name: 'Cairo' },
		  { id: 5, name: 'Chicago' },
		  { id: 6, name: 'Lima' },
		  { id: 7, name: 'London' },
		  { id: 8, name: 'Miami' },
		  { id: 9, name: 'Moscow' },
		  { id: 10, name: 'Mumbai' },
		  { id: 11, name: 'Paris' },
		  { id: 12, name: 'San Francisco' }
		];


		console.log(mockData);
		console.log('real array');
		console.log(questions);
		this.dispatch(questions);
	}
	fetchQuestions() {
	    // we dispatch an event here so we can have "loading" state.
	    // this.dispatch();
	    QuestionWallSource.fetch()
	      .then((questions) => {
	        // we can access other actions within our action through `this.actions`
	       this.actions.updateQuestions(questions);
	      })
	      .catch((errorMessage) => {

	      });

	}
}

module.exports = alt.createActions(QuestionWallActions);
