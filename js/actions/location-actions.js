var alt = require('../alt');
var LocationSource = require('../sources/location-source');
var $ = require('jquery');

var siteUrl = "http://localhost/project/wp-content/themes/project/alt/documentation/js/sources/php/uploads/";


class LocationActions {
	updateLocations(locations) {
		this.dispatch(locations);
	}
	fetchLocations() {
	    // we dispatch an event here so we can have "loading" state.
	    this.dispatch();
	    LocationSource.fetch()
	      .then((locations) => {
	        // we can access other actions within our action through `this.actions`
	        this.actions.updateLocations(locations);
	      })
	      .catch((errorMessage) => {
	        this.actions.locationsFailed(errorMessage);
	      });
	}
	createLocation(name) {
		LocationSource.createLocation(name)
		.then((locations) => {
	        // we can access other actions within our action through `this.actions`
	        this.actions.fetchLocations();
	    })
	    .catch((errorMessage) => {
	        this.actions.locationsFailed(errorMessage);
	    });
	}
	deleteLocation(id) {
		LocationSource.delLocation(id)
		.then((locations) => {
	        // we can access other actions within our action through `this.actions`
	        this.actions.fetchLocations();
	    })
	    .catch((errorMessage) => {
	        this.actions.locationsFailed(errorMessage);
	    });
	}	
	locationsFailed(errorMessage) { 
	    this.dispatch(errorMessage);
	}
	setFavorite(id) {
		LocationSource.updateLocation(id)
		.then((locations) => {
	        // we can access other actions within our action through `this.actions`
	        this.actions.updateLocations(locations);
	      })
	      .catch((errorMessage) => {
	        this.actions.locationsFailed(errorMessage);
	      });
	}
	favoriteLocation(locations) {
	  this.dispatch(locations);
	}
	updateImageOne(files) {

		
			this.dispatch(files);
		
	}
	updateImageTwo(files) {


		
			this.dispatch(files);
		
	}	
	uploadingImgOne(files) {
		// var test = [{'files': files, 'stuff': imageNum}];

		//test.push(files);
		//test.push('imageNum': imageNum);
		
		// console.log(test);
		LocationSource.uploadImage(files)
	      .then((files) => {
	        // we can access other actions within our action through `this.actions`
	        this.actions.updateImageOne(files);
	      })
	      .catch((errorMessage) => {
	        // this.actions.locationsFailed(errorMessage);
	      });

			

		
		

	// Create a formdata object and add the files
	   
	    	
	}
	uploadingImgTwo(files) {
		// var test = [{'files': files, 'stuff': imageNum}];

		//test.push(files);
		//test.push('imageNum': imageNum);
		
		// console.log(test);
		LocationSource.uploadImage(files)
	      .then((files) => {
	        // we can access other actions within our action through `this.actions`
	        this.actions.updateImageTwo(files);
	      })
	      .catch((errorMessage) => {
	        // this.actions.locationsFailed(errorMessage);
	      });

	// Create a formdata object and add the files
	   
	    	
	}
	createQuestion(formData) {
		LocationSource.createQuestion(formData)
		.then((formData) => {
	        // we can access other actions within our action through `this.actions`
	        console.log('check your db!');

	    })
	    .catch((errorMessage) => {
	        this.actions.locationsFailed(errorMessage);
	    });
	}	
}

module.exports = alt.createActions(LocationActions);