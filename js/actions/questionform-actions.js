var alt = require('../alt');
var QuestionFormSource = require('../sources/questionform-source');
var $ = require('jquery');
var siteUrl = "http://localhost/project/wp-content/themes/project/alt/fx/js/sources/php/uploads/";


class QuestionFormActions {
	updateCaption(text){
		this.dispatch(text)
	}	
	uploadingImgOne(files) {
		QuestionFormSource.uploadImage(files)
	      .then((files) => {
	        // we can access other actions within our action through `this.actions`
	        this.dispatch(files);
	      })
	      .catch((errorMessage) => {
	        // this.actions.locationsFailed(errorMessage);
	      });    	
	}
	uploadingImgTwo(files) {
		QuestionFormSource.uploadImage(files)
	      .then((files) => {
	        // we can access other actions within our action through `this.actions`
	        this.dispatch(files);
	      })
	      .catch((errorMessage) => {
	        // this.actions.locationsFailed(errorMessage);
	      });   	
	}
	createQuestion(formData) {
		QuestionFormSource.createQuestion(formData)
		.then((formData) => {
	        // we can access other actions within our action through `this.actions`
	        console.log('check your db!');
	        this.dispatch();
	    })
	    .catch((errorMessage) => {
	        this.actions.locationsFailed(errorMessage);
	    });

	}	
}

module.exports = alt.createActions(QuestionFormActions);