var React = require('react');
var LocationActions = require('../actions/location-actions');
var LocationStore = require('../stores/location-store');

var Favorites = React.createClass({
  render() {
    return (
      <ul>
        {this.props.locations.map((location, i) => {
          return (
            <li key={i}>{location.name}</li>
          );
        })}
      </ul>
    );
  }
});

module.exports = Favorites;