var React = require('react');
var AltContainer = require('alt/AltContainer');
var LocationActions = require('../actions/location-actions');
var LocationStore = require('../stores/location-store');
var FavouritesStore = require('../stores/favourite-store');

var Dropzone = require('react-dropzone');

var ENTER_KEY_CODE = 13;

var QuestionForm = React.createClass({
   _onChange: function(event, value) {
    this.setState({locationName: event.target.value});
  }, 
  _onKeyDown: function(event) {
    if (event.keyCode === ENTER_KEY_CODE) {
      var name = this.state.locationName.trim();
      if (name) {
        LocationActions.createLocation(name);
      }
      this.setState({locationName: ''});
    }
  },
  getInitialState() {
    return LocationStore.getState();
  },
  onDrop(files) {
    LocationActions.uploadingImgOne(files); 
  },
  onDropTwo(files) {
    LocationActions.uploadingImgTwo(files); 
  },  
  onSubmitQuestion(event) {
    event.preventDefault();
    var formData = [{
      'caption': this.state.locationName,
      'imgOne': this.state.imgOne[0].name,
      'imgTwo': this.state.imgTwo[0].name
    }]
    LocationActions.createQuestion(formData);
    this.setState({
      locationName: '',
      imgOne: [],
      imgTwo: []
    });
  },
  render() {
    return (
      <div className="form-container">
          <div className="row">
            <input ref="hh" onChange={this._onChange} onKeyDown={this._onKeyDown} type="text" value={this.state.locationName} />
            <div className="img-preview one small-6 columns">
              <div>
              {this.state.imgOne.length > 0 ?
                <img src={'http://localhost/project/wp-content/themes/project/alt/documentation/js/sources/php/uploads/' + this.state.imgOne[0].name} />
                : 
                <Dropzone ref="dropzoneOne" onDrop={this.onDrop}>
                  <div>Upload Image One</div>
                </Dropzone>}
              </div>                  
            </div>
            <div className="img-preview two small-6 columns">
              <div>
              {this.state.imgTwo.length > 0 ?
                <img src={"http://localhost/project/wp-content/themes/project/alt/documentation/js/sources/php/uploads/" + this.state.imgTwo[0].name} />
                : 
                <Dropzone ref="dropzoneTwo" onDrop={this.onDropTwo}>
                  <div>Upload Image Two</div>
                </Dropzone> }
              </div>                  
            </div> 
            <div className="small-12 columns">
              <button type="submit" onClick={this.onSubmitQuestion} >Submit</button>   
            </div> 
          </div>     
      </div>
    )
  }

});

var Favorites = React.createClass({
  render() {
    return (
      <ul>
        {this.props.favLocations.map((location, i) => {
         	return (
            <li key={i}>{location.name}</li>
          );
        })}
      </ul>
    );
  }
});

var AllLocations = React.createClass({
  addFave(ev) {
    var locationID = Number(ev.target.getAttribute('data-id'));
    console.log(locationID);
    // var location = LocationStore.getLocation(fxtest);
    // console.log(location);
    LocationActions.setFavorite(locationID);
  },
  delLocation(ev) {
    var locationID = Number(ev.target.getAttribute('data-id'));

    LocationActions.deleteLocation(locationID);
  },  

  render() {
    if (this.props.errorMessage) {
      return (
        <div>{this.props.errorMessage}</div>
      );
    }  



    return (
      <ul>
        {this.props.locations.map((location, i) => {
          var faveButton = (
            <button onClick={this.addFave} data-id={location.id}>
              Favorite
            </button>
          );
          var delButton = (
            <button onClick={this.delLocation} data-id={location.id}>
              Delete
            </button>
          );          
          return (
            <li key={i}>
              {location.name} {location.has_favorite ? '<3' : faveButton} {delButton}
            </li>
          );
        })}
      </ul>
    );
  }
});

var Locations = React.createClass({
	getInitialState() {
	  return LocationStore.getState();
	},
	componentDidMount() {
	  LocationStore.listen(this.onChange);

	  LocationActions.fetchLocations();
	},
	componentWillUnmount() {
	  LocationStore.unlisten(this.onChange);
	},
	onChange(state) {
		this.setState(state);
	},
	render() {
	    return (
			<div>
		        <h1>Locations</h1>
		        <QuestionForm />
            <AltContainer store={LocationStore}>
		          <AllLocations />
		        </AltContainer>

		        <h1>Favorites</h1>
		        <AltContainer store={LocationStore}>
		          <Favorites />
		        </AltContainer>
		    </div>
	    );
	}
});
 
module.exports = Locations;