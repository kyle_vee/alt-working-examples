var React = require('react');
var QuestionFormActions = require('../actions/questionform-actions');
var QuestionFormStore = require('../stores/questionform-store');
var Dropzone = require('react-dropzone');


var QuestionForm = React.createClass({
   _onChange: function(event, value) {
    var text = event.target.value;
    QuestionFormActions.updateCaption(text);
  }, 
  getInitialState() {
    return QuestionFormStore.getState();
  },
  onDrop(files) {
    QuestionFormActions.uploadingImgOne(files); 
  },
  onDropTwo(files) {
    QuestionFormActions.uploadingImgTwo(files); 
  },  
  onSubmitQuestion(event) {
    event.preventDefault();
    var formData = [{
      'caption': this.props.locationName,
      'imgOne': this.props.imgOne[0].name,
      'imgTwo': this.props.imgTwo[0].name
    }]
    QuestionFormActions.createQuestion(formData);
    /*
    this.setState({
      locationName: '',
      imgOne: [],
      imgTwo: []
    });
*/
  },
  render() {
    return (
        <div className="form-container">
            <div className="row">
              <input ref="hh" onChange={this._onChange} type="text" value={this.props.locationName} />
              <div className="img-preview one small-6 columns">
                <div>
                {this.props.imgOne.length > 0 ?
                  <img src={'http://localhost/project/wp-content/themes/project/alt/documentation/js/sources/php/uploads/' + this.props.imgOne[0].name} />
                  : 
                  <Dropzone ref="dropzoneOne" onDrop={this.onDrop}>
                    <div>Upload Image One</div>
                  </Dropzone>}
                </div>                  
              </div>
              <div className="img-preview two small-6 columns">
                <div>
                {this.props.imgTwo.length > 0 ?
                  <img src={"http://localhost/project/wp-content/themes/project/alt/documentation/js/sources/php/uploads/" + this.props.imgTwo[0].name} />
                  : 
                  <Dropzone ref="dropzoneTwo" onDrop={this.onDropTwo}>
                    <div>Upload Image Two</div>
                  </Dropzone> }
                </div>                  
              </div> 
              <div className="small-12 columns">
                <button type="submit" onClick={this.onSubmitQuestion} >Submit</button>   
              </div> 
            </div>     
        </div>
    )
  }

});


 
module.exports = QuestionForm;