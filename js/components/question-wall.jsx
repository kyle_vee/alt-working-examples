var React = require('react');
var QuestionWallActions = require('../actions/questionwall-actions');
var QuestionWallStore = require('../stores/questionform-store');
var Dropzone = require('react-dropzone');


var QuestionWall = React.createClass({
  getInitialState() {
    return QuestionWallStore.getState();
  },
  componentWillMount() {
    QuestionWallActions.fetchQuestions();
  },
  onGetQuestion() {
    
  },
  render() {
    return (
        <div className="question-wall"> 
          {this.props.allQuestions.map((question, i) => {         
            return (
              <div key={i} className="panel">
                {question.caption} <br />
                {question.img_one} <br />
                {question.img_two}
              </div>
            );
          })}     
        </div>
    )
  }

});


 
module.exports = QuestionWall;