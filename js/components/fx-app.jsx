var React = require('react');
var AltContainer = require('alt/AltContainer');
// getting the different components we want to make up our app as a whole
var QuestionForm = require('./question-form.jsx');
var QuestionWall = require('./question-wall.jsx');
var QuestionFormStore = require('../stores/questionform-store');
var QuestionWallActions = require('../actions/questionwall-actions');
// var ThreadSection = require('./ThreadSection.react');



var FxApp = React.createClass({
  render: function() {
    return (
      <div className="fxapp">
        <AltContainer store={QuestionFormStore}>
          <QuestionForm />
          <QuestionWall />
        </AltContainer>  
      </div>
    );
  }

});

module.exports = FxApp;